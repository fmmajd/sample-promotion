all: say_hello

say_hello:
	@echo "Hello World!"

up:
	@docker-compose up -d

down:
	@docker-compose down --remove-orphans

restart:
	@docker-compose restart

build:
	@docker-compose exec wallet bash -c "go build main.go"
	@docker-compose exec promotion bash -c "go build main.go"

run_promotion:
	@docker-compose exec promotion bash -c "go run main.go"

run_wallet:
	@docker-compose exec wallet bash -c "go run main.go"

bash_promotion:
	@docker-compose exec promotion bash

bash_wallet:
	@docker-compose exec wallet bash