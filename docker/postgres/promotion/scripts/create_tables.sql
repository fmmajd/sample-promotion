CREATE TABLE IF NOT EXISTS promotions (
     id              bigserial primary key,
     code            varchar unique not null,
     amount          int not null check ( amount > 0 )
);

CREATE TABLE IF NOT EXISTS usages (
     id              bigserial primary key,
     code            varchar not null references promotions(code) on delete cascade,
     number          bigint not null check ( number > 999999999 and number < 10000000000 ),
     unique (number, code)
);
