CREATE TABLE IF NOT EXISTS users (
     id              bigserial primary key,
     number          bigint unique not null check ( number > 999999999 and number < 10000000000 ),
     balance         bigint not null default 0 check ( balance >= 0 )
);

CREATE TABLE IF NOT EXISTS transactions (
    id              bigserial primary key,
    user_id         bigint references users(id),
    description     varchar not null,
    change          bigint not null
);