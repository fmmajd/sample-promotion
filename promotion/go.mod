module gitlab.com/fmmajd/sample-promotion/promotion

go 1.13

require (
	github.com/fmmajd/gopostgres v0.0.3
	github.com/jackc/pgx/v4 v4.4.1
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	golang.org/x/crypto v0.0.0-20200221231518-2aa609cf4a9d // indirect
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543 // indirect
)
