package main

import (
	"github.com/fmmajd/gopostgres"
	"github.com/jackc/pgx/v4"
	"gitlab.com/fmmajd/sample-promotion/promotion/promotions"
	"gitlab.com/fmmajd/sample-promotion/promotion/server"
	"log"
	"net/http"
	"os"
	"strconv"
)

func main() {
	initPostgres()
	code, codeIsSet := os.LookupEnv("PROMOTION_CODE")
	if !codeIsSet {
		code = "arvan2020"
	}

	val, valIsSet := os.LookupEnv("PROMOTION_VALUE")
	var value uint
	if !valIsSet {
		value = 10000
	} else {
		valueInt,err := strconv.Atoi(val)
		if err != nil {
			log.Fatalln("value of the promotion code is invalid")
		}
		value = uint(valueInt)
	}

	err := promotions.AddIfNotExists(code, value)
	if err != nil {
		log.Fatalln(err)
	}

	server.InitCounter()

	http.HandleFunc("/charge", server.ChargeHandler)
	http.HandleFunc("/list/", server.ListHandler)
	if err = http.ListenAndServe(":8080", nil); err != nil {
		log.Fatalln(err)
	}
}

func initPostgres(){
	db, isSet := os.LookupEnv("PROMOTION_DB_DB")
	if !isSet {
		log.Fatalln("promotion db not found in environment")
	}
	user, isSet := os.LookupEnv("PROMOTION_DB_USER")
	if !isSet {
		log.Fatalln("promotion database user not found in environment")
	}
	password, isSet := os.LookupEnv("PROMOTION_DB_PASSWORD")
	if !isSet {
		log.Fatalln("promotion database password not found in environment")
	}
	gopostgres.InitDB(db, user, password, "promotion_db", gopostgres.StdLogger{}, pgx.LogLevelWarn)
}
