package promotions

func AddIfNotExists(code string, amount uint) error{
	if Exists(code) {
		return nil
	}

	g := gift{
		id:     0,
		code:   code,
		amount: amount,
	}
	err := g.Save()
	if err != nil {
		return err
	}

	return nil
}
