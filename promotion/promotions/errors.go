package promotions

import "fmt"

type UserAlreadyConsumedTheCode struct{
	code string
	number uint
}

func (e UserAlreadyConsumedTheCode) Error() string {
	return fmt.Sprintf("number:%d has already consumed the code:%s", e.number, e.code)
}
