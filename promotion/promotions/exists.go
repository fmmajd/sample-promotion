package promotions

import (
	"github.com/fmmajd/gopostgres"
)

func Exists(code string) bool {
	_, err := gopostgres.DB.FindBy(giftsTable, "code", code)
	if err != nil {
		return false
	}

	return true
}
