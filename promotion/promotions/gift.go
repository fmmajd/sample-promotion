package promotions

import (
	"github.com/fmmajd/gopostgres"
)

var giftsTable = "promotions"

type gift struct {
	id uint
	code string
	amount uint
}

func (g gift) PostgresTable() string {
	return giftsTable
}

func (g gift) PostgresColumns() []string {
	return []string {
		"id",
		"gift",
		"amount",
	}
}

func (g gift) PostgresValue(column string) interface{} {
	switch column {
	case "id":
		return g.id
	case "code":
		return g.code
	case "amount":
		return g.amount
	default:
		return nil
	}
}

func (g gift) PostgresValues() map[string]interface{} {
	return map[string]interface{} {
		"code": g.PostgresValue("code"),
		"amount": g.PostgresValue("amount"),
	}
}

func (g gift) PostgresId() uint {
	return g.id
}

func (g *gift) Save() error{
	if g.id <= 0 {
		id, err := gopostgres.DB.Insert(g)
		if err != nil {
			return err
		}
		g.id = id
		return nil
	} else {
		return gopostgres.DB.Update(g)
	}
}

func PromotionValueByCode(code string) (int, error) {
	res, err := gopostgres.DB.FindBy(giftsTable, "code", code)
	if err != nil {
		return 0, err
	}

	amount := res[2].(int32)
	return int(amount), nil
}