package promotions

import "github.com/fmmajd/gopostgres"

var usageTable = "usages"

type usage struct {
	id uint
	code string
	number uint
}

func (u usage) PostgresTable() string {
	return usageTable
}

func (u usage) PostgresColumns() []string {
	return []string{
		"id",
		"code",
		"number",
	}
}

func (u usage) PostgresValue(column string) interface{} {
	switch column {
	case "id":
		return u.id
	case "code":
		return u.code
	case "number":
		return u.number
	default:
		return nil
	}
}

func (u usage) PostgresValues() map[string]interface{} {
	return map[string]interface{}{
		"code": u.PostgresValue("code"),
		"number": u.PostgresValue("number"),
	}
}

func (u usage) PostgresId() uint {
	return u.id
}

func (u *usage) Save() error{
	if u.id <= 0 {
		id, err := gopostgres.DB.Insert(u)
		if err != nil {
			return err
		}
		u.id = id
		return nil
	} else {
		return gopostgres.DB.Update(u)
	}
}

func CodeUsageCount(code string) (int, error) {
	res, err := gopostgres.DB.FindAllBy(usageTable, "code", code)
	if err != nil {
		switch err.(type) {
		case gopostgres.NoRecordFound:
			return 0, nil
		default:
			return -1, err
		}
	}
	return len(res), nil
}
