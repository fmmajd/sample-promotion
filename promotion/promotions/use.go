package promotions

import (
	"github.com/fmmajd/gopostgres"
)

func HasUsed(number uint, code string) (bool, error){
	prev, err := gopostgres.DB.FindAllWhere(usageTable, []string{"id"}, gopostgres.WhereEquals("code", code), gopostgres.WhereEquals("number", number))
	if err != nil {
		return true, err
	}
	if len(prev) > 0 {
		return true, nil
	}
	return false, nil
}

func Use(number uint, code string) error{
	u := usage{
		id: 0,
		code: code,
		number: number,
	}

	return u.Save()
}
