package queue

import (
	"github.com/streadway/amqp"
	"log"
)

var channel *amqp.Channel

func theChannel() *amqp.Channel{
	if channel != nil {
		return channel
	} else {
		chann, err := connection.Channel()
		if err != nil {
			log.Fatalln(err)
		}
		channel = chann
		return channel
	}
}
