package queue

import (
	"fmt"
	"github.com/streadway/amqp"
	"log"
	"os"
)

var connection amqp.Connection

func init() {
	initConnection()
	declareQueues()
}

func initConnection() {
	var isSet bool
	var user, password, port string
	user, isSet = os.LookupEnv("RABBITMQ_USER")
	if !isSet {
		log.Fatalln("rabbit user was not found")
	}
	password, isSet = os.LookupEnv("RABBITMQ_PASSWORD")
	if !isSet {
		log.Fatalln("rabbit password was not found")
	}
	port, isSet = os.LookupEnv("RABBITMQ_PORT")
	if !isSet {
		log.Fatalln("rabbit port was not found")
	}
	connectionUrl := fmt.Sprintf("amqp://%s:%s@rabbitmq:%s/", user, password, port)
	conn, err := amqp.Dial(connectionUrl)
	if err != nil {
		log.Fatalln(err)
	}
	connection = *conn
}
