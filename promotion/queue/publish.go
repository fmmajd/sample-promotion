package queue

import (
	"encoding/json"
	"github.com/streadway/amqp"
)

func PublishPromotion(number uint, change int, description string) error{
	data := map[string]interface{}{
		"change": change,
		"number": number,
		"description": description,
	}
	body, err := json.Marshal(data)
	if err != nil {
		return err
	}
	chann := theChannel()
	err = chann.Publish(
		"",     // exchange
		PromotionQueueName,
		true,  // mandatory
		false,  // immediate
		amqp.Publishing {
			ContentType: "text/plain",
			Body:        body,
		})

	return err
}
