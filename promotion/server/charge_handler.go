package server

import (
	"encoding/json"
	"fmt"
	"gitlab.com/fmmajd/sample-promotion/promotion/promotions"
	"gitlab.com/fmmajd/sample-promotion/promotion/queue"
	"log"
	"net/http"
)

type chargeInput struct {
	Code string `json:"code"`
	Number uint `json:"number"`
}

func ChargeHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "method not allowed", http.StatusMethodNotAllowed)
		return
	}

	var input chargeInput
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		createErrorResponse(w,
			"could not decode body",
			map[string]interface{}{"error": err.Error()},
			http.StatusBadRequest)

		return
	}

	if !promotions.Exists(input.Code) {
		createErrorResponse(w,
			"code not found",
			map[string]interface{}{"code": input.Code},
			http.StatusBadRequest)
		return
	}

	hasUsed, err := promotions.HasUsed(input.Number, input.Code)
	if err != nil {
		createErrorResponse(w,
			"could not check your last usages",
			map[string]interface{}{},
			http.StatusBadRequest)
		return
	}
	if hasUsed {
		createErrorResponse(w,
			"you've already used this code",
			map[string]interface{}{},
			http.StatusBadRequest)
		return
	}

	if LimitationPassed() {
		createErrorResponse(w,
			"all the existing codes are used, sorry",
			map[string]interface{}{},
			http.StatusBadRequest)
		return
	}

	err = promotions.Use(input.Number, input.Code)
	if err != nil {
		createErrorResponse(w,
			"could not use this coupon for you",
			map[string]interface{}{},
			http.StatusBadRequest)
		return
	}
	AddToCounter()

	amount, err := promotions.PromotionValueByCode(input.Code)
	if err != nil {
		createErrorResponse(w,
			"could not find the value of this coupon",
			map[string]interface{}{},
			http.StatusBadRequest)
		return
	}
	go func() {
		counter := 0
		for {
			var err error
			err = queue.PublishPromotion(input.Number, amount, fmt.Sprintf("promotion code %s", input.Code))
			if err == nil {
				break
			}
			counter++
			if counter >= 10 {
				log.Printf("Could not queue code %s for user %d\n", input.Code, input.Number)
				break
			}
		}
	} ()


	createSuccessResponse(w, "done", nil, http.StatusOK)
	return
}
