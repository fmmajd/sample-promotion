package server

import "sync"

var codeCounter counter

const limitaion = 10000

type counter struct {
	lock sync.Mutex
	val int
}

func (c *counter) add(step int) {
	c.lock.Lock()
	c.val = c.val+step
	c.lock.Unlock()
}

func InitCounter() {
	codeCounter = counter{
		val:  0,
	}
}

func AddToCounter() {
	codeCounter.add(1)
}

func LimitationPassed() bool{
	return codeCounter.val >= limitaion
}

