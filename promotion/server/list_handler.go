package server

import (
	"gitlab.com/fmmajd/sample-promotion/promotion/promotions"
	"net/http"
)

func ListHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, "method not allowed", http.StatusMethodNotAllowed)
		return
	}

	code := r.URL.Path[len("/list/"):]

	exists := promotions.Exists(code)
	if !exists {
		createErrorResponse(w,
			"code does not exist",
			map[string]interface{}{},
			http.StatusNotFound)
		return
	}

	cnt, err := promotions.CodeUsageCount(code)
	if err != nil {
		createErrorResponse(w,
			"could not calculate the list",
			map[string]interface{}{},
			http.StatusBadRequest)
		return
	}

	createSuccessResponse(w, "done", map[string]interface{}{
		"count": cnt,
	}, http.StatusOK)
}
