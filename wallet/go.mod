module gitlab.com/fmmajd/sample-promotion/wallet

go 1.13

require (
	github.com/fmmajd/gopostgres v0.0.3
	github.com/jackc/pgx/v4 v4.4.1
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
)
