package main

import (
	"github.com/fmmajd/gopostgres"
	"github.com/jackc/pgx/v4"
	"gitlab.com/fmmajd/sample-promotion/wallet/queue"
	"gitlab.com/fmmajd/sample-promotion/wallet/server"
	"log"
	"net/http"
	"os"
)

func main() {
	initPostgres()
	queue.Init()

	queue.ListenToPromotions()

	http.HandleFunc("/balance/", server.BalanceHandler)
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatalln(err)
	}
}

func initPostgres(){
	db, isSet := os.LookupEnv("WALLET_DB_DB")
	if !isSet {
		log.Fatalln("wallet db not found in environment")
	}
	user, isSet := os.LookupEnv("WALLET_DB_USER")
	if !isSet {
		log.Fatalln("wallet database user not found in environment")
	}
	password, isSet := os.LookupEnv("WALLET_DB_PASSWORD")
	if !isSet {
		log.Fatalln("wallet database password not found in environment")
	}
	gopostgres.InitDB(db, user, password, "wallet_db", gopostgres.StdLogger{}, pgx.LogLevelWarn)
}
