package queue

import (
	"encoding/json"
	"gitlab.com/fmmajd/sample-promotion/wallet/transaction"
	"log"
)

type receivedQueueMessage struct {
	Number uint
	Change int
	Description string
}

func ListenToPromotions() {
	chann := theChannel()
	messages, err := chann.Consume(
		PromotionQueueName,
		"",     // consumer
		false,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	if err != nil {
		log.Fatalln(err)
	}

	go func() {
		for m := range messages {
			var body receivedQueueMessage
			err := json.Unmarshal(m.Body, &body)
			if err != nil {
				log.Println(err)
				m.Nack(false, true)
				continue
			}
			go func() {
				err = transaction.ChangeBalance(body.Number, body.Change, body.Description)
				if err != nil {
					log.Println(err)
					m.Nack(false, true)
				} else {
					m.Ack(false)
				}
			} ()
		}
	}()
}
