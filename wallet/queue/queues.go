package queue

import "log"

var PromotionQueueName = "promotions"

func declareQueues() {
	chann := theChannel()
	_, err := chann.QueueDeclare(
		PromotionQueueName, // name
		true,   // durable
		false,   // delete when unused
		false,   // exclusive
		false,   // no-wait
		nil,     // arguments
	)
	if err != nil {
		log.Fatalln(err)
	}
}
