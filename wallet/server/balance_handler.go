package server

import (
	"gitlab.com/fmmajd/sample-promotion/wallet/transaction"
	"log"
	"net/http"
	"strconv"
)

func BalanceHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, "method not allowed", http.StatusMethodNotAllowed)
		return
	}

	numberStr := r.URL.Path[len("/balance/"):]
	numberInt, err := strconv.Atoi(numberStr)
	if err != nil {
		createErrorResponse(w,
			"number is not valid",
			map[string]interface{}{},
			http.StatusBadRequest)
		return
	}
	number := uint(numberInt)

	exists := transaction.UserExistsByNumber(number)
	var balance int
	if exists {
		u, err := transaction.UserByNumber(number)
		if err != nil {
			log.Println(err)
			createErrorResponse(w,
				"could not find the user",
				map[string]interface{}{},
				http.StatusBadRequest)
			return
		}
		balance = u.Balance()
	} else {
		balance = 0
	}

	createSuccessResponse(w, "done", map[string]interface{}{
		"balance": balance,
	}, http.StatusOK)
}
