package server

import (
	"encoding/json"
	"net/http"
)

type response struct {
	Message string
	Data map[string]interface{}
	Success bool
}

func (r response) jsonEncoded() []byte {
	jj, err := json.Marshal(r)
	if err != nil {
		return []byte("O_O")
	}
	return jj
}

func (r response) create(w http.ResponseWriter, status int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write(r.jsonEncoded())
}

func createSuccessResponse(w http.ResponseWriter, message string, data map[string]interface{}, status int) {
	r := response{
		Message: message,
		Data:    data,
		Success: true,
	}
	r.create(w, status)
}

func createErrorResponse(w http.ResponseWriter,
	message string,
	data map[string]interface{},
	status int) {
	r := response{
		Message: message,
		Data:    data,
		Success: false,
	}
	r.create(w, status)
}