package transaction

func ChangeBalance(number uint, change int, description string) error{
	var u User
	var err error
	if !UserExistsByNumber(number) {
		u = User{
			id: 0,
			number: number,
			balance: 0,
		}
		err = u.Save()
		if err != nil {
			return err
		}
	} else {
		uPtr, err := UserByNumber(number)
		if err != nil {
			return err
		}
		u = *uPtr
	}

	//TODO: use transactions in the gopostgres package
	t := transaction{
		id: 0,
		userId: u.id,
		description: description,
		change: change,
	}
	err = t.Save()
	if err != nil {
		return err
	}
	u.balance = u.balance+change
	err = u.Save()
	if err != nil {
		return err
	}

	return nil
}