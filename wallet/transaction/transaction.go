package transaction

import "github.com/fmmajd/gopostgres"

var transactionsTable = "transactions"

type transaction struct {
	id uint
	userId uint
	description string
	change int
}

func (t transaction) PostgresTable() string{
	return transactionsTable
}

func (t transaction) PostgresColumns() []string{
	return []string{
		"id",
		"user_id",
		"description",
		"change",
	}
}

func (t transaction) PostgresValue(column string) interface{}{
	switch column {
	case "id":
		return t.id
	case "user_id":
		return t.userId
	case "change":
		return t.change
	case "description":
		return t.description
	default:
		return nil
	}
}

func (t transaction) PostgresValues() map[string]interface{}{
	return map[string]interface{}{
		"user_id": t.PostgresValue("user_id"),
		"change": t.PostgresValue("change"),
		"description": t.PostgresValue("description"),
	}
}

func (t transaction) PostgresId() uint{
	return t.id
}

func (t *transaction) Save() error {
	if t.id <= 0 {
		id, err := gopostgres.DB.Insert(t)
		if err != nil {
			return err
		}
		t.id = id
		return nil
	} else {
		return gopostgres.DB.Update(t)
	}
}

