package transaction

import "github.com/fmmajd/gopostgres"

var usersTable = "users"

type User struct {
	id uint
	number uint
	balance int
}

func (u User) PostgresTable() string{
	return usersTable
}

func (u User) PostgresColumns() []string{
	return []string{
		"id",
		"number",
		"balance",
	}
}

func (u User) PostgresValue(column string) interface{}{
	switch column {
	case "id":
		return u.id
	case "number":
		return u.number
	case "balance":
		return u.balance
	default:
		return nil
	}
}

func (u User) PostgresValues() map[string]interface{}{
	return map[string]interface{}{
		"number": u.PostgresValue("number"),
		"balance": u.PostgresValue("balance"),
	}
}

func (u User) PostgresId() uint{
	return u.id
}

func (u *User) Save() error {
	if u.id <= 0 {
		id, err := gopostgres.DB.Insert(u)
		if err != nil {
			return err
		}
		u.id = id
		return nil
	} else {
		return gopostgres.DB.Update(u)
	}
}

func (u User) Balance() int {
	return u.balance
}

func UserByNumber(number uint) (*User, error){
	res, err := gopostgres.DB.FindBy(usersTable, "number", number)
	if err != nil {
		return nil, err
	}
	id := uint(res[0].(int64))
	balance := int(res[2].(int64))
	u := User{
		id:      id,
		number:  number,
		balance: balance,
	}

	return &u, nil
}

func UserExistsByNumber(number uint) bool {
	_, err := gopostgres.DB.FindBy(usersTable, "number", number)
	if err != nil {
		return false
	}

	return true
}
