# github.com/fmmajd/gopostgres v0.0.3
github.com/fmmajd/gopostgres
# github.com/jackc/chunkreader/v2 v2.0.1
github.com/jackc/chunkreader/v2
# github.com/jackc/pgconn v1.3.2
github.com/jackc/pgconn
github.com/jackc/pgconn/internal/ctxwatch
github.com/jackc/pgconn/stmtcache
# github.com/jackc/pgio v1.0.0
github.com/jackc/pgio
# github.com/jackc/pgpassfile v1.0.0
github.com/jackc/pgpassfile
# github.com/jackc/pgproto3/v2 v2.0.1
github.com/jackc/pgproto3/v2
# github.com/jackc/pgtype v1.2.0
github.com/jackc/pgtype
# github.com/jackc/pgx/v4 v4.4.1
github.com/jackc/pgx/v4
github.com/jackc/pgx/v4/internal/sanitize
# github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
github.com/streadway/amqp
# golang.org/x/crypto v0.0.0-20190911031432-227b76d455e7
golang.org/x/crypto/pbkdf2
# golang.org/x/text v0.3.2
golang.org/x/text/cases
golang.org/x/text/internal
golang.org/x/text/internal/language
golang.org/x/text/internal/language/compact
golang.org/x/text/internal/tag
golang.org/x/text/language
golang.org/x/text/runes
golang.org/x/text/secure/bidirule
golang.org/x/text/secure/precis
golang.org/x/text/transform
golang.org/x/text/unicode/bidi
golang.org/x/text/unicode/norm
golang.org/x/text/width
# golang.org/x/xerrors v0.0.0-20190717185122-a985d3407aa7
golang.org/x/xerrors
golang.org/x/xerrors/internal
